#include <Arduino.h>
#include <Sprite.h>  // Sprite before Matrix
#include <Matrix.h>
#include <MIDI.h>
#include <main.h>

#define UP			2
#define DOWN		3
#define BUTTON1		4
#define BUTTON2		5
#define BUTTON3		6

#define DEBOUNCE_TIME	100

#define MIDI_CHANNEL	1

uint8_t bank = 0;
const uint8_t maxBank = (sizeof(chars)/sizeof(chars[0])) - 1;
bool needRedraw = true;

NamedButton up = NamedButton("BANK UP", UP, true, true, 200);
NamedButton dn = NamedButton("BANK DOWN", DOWN, true, true, 200);
NamedButton b1 = NamedButton("BUTTON 1", BUTTON1, true, true, 200);
NamedButton b2 = NamedButton("BUTTON 2", BUTTON2, true, true, 200);
NamedButton b3 = NamedButton("BUTTON 3", BUTTON3, true, true, 200);

//Parameters: SDA, SCL, CS, number of matrices
Matrix matrix = Matrix(18, 19, 1, 1);

#if defined(USBCON)
#include <midi_UsbTransport.h>

static const unsigned sUsbTransportBufferSize = 16;
typedef midi::UsbTransport<sUsbTransportBufferSize> UsbTransport;

UsbTransport sUsbTransport;

MIDI_CREATE_INSTANCE(UsbTransport, sUsbTransport, MIDI);

#else // No USB available, fallback to Serial
MIDI_CREATE_DEFAULT_INSTANCE();
#endif

/************************************************************
 * SINGLE PRESS												*
 ***********************************************************/
void handlerUpClicked() {
	Serial3.println("Up pressed");

	if (bank >= maxBank-1) {
		bank = 0;
	} else {
		bank++;
	}
	needRedraw = true;
}

void handlerDownClicked() {
	Serial3.println("Down pressed");

	if (bank <= 0) {
		bank = maxBank;
	} else {
		bank--;
	}
	needRedraw = true;
}

void handlerButton1Clicked() {
	Serial3.println("Button 1 pressed");

	usbMIDI.sendNoteOn((bank*9)+1, 99, MIDI_CHANNEL);
	usbMIDI.sendNoteOff((bank*9)+1, 0, MIDI_CHANNEL);
}

void handlerButton2Clicked() {
	Serial3.println("Button 2 pressed");

	usbMIDI.sendNoteOn((bank*9)+2, 99, MIDI_CHANNEL);
	usbMIDI.sendNoteOff((bank*9)+2, 0, MIDI_CHANNEL);
}

void handlerButton3Clicked() {
	Serial3.println("Button 3 pressed");

	usbMIDI.sendNoteOn((bank*9)+3, 99, MIDI_CHANNEL);
	usbMIDI.sendNoteOff((bank*9)+3, 0, MIDI_CHANNEL);
}

/************************************************************
 * DOUBLE PRESS												*
 ***********************************************************/
void handlerUpDoubleClicked() {
	Serial3.println("Up Double pressed");

	bank = maxBank;
	needRedraw = true;
}

void handlerDownDoubleClicked() {
	Serial3.println("Down Double pressed");

	bank = 0;
	needRedraw = true;
}

void handlerButton1DoubleClicked() {
	Serial3.println("Button 1 Double pressed");

	usbMIDI.sendNoteOn((bank*9)+4, 99, MIDI_CHANNEL);
	usbMIDI.sendNoteOff((bank*9)+4, 0, MIDI_CHANNEL);
}

void handlerButton2DoubleClicked() {
	Serial3.println("Button 2 Double pressed");

	usbMIDI.sendNoteOn((bank*9)+5, 99, MIDI_CHANNEL);
	usbMIDI.sendNoteOff((bank*9)+5, 0, MIDI_CHANNEL);
}

void handlerButton3DoubleClicked() {
	Serial3.println("Button 3 Double pressed");

	usbMIDI.sendNoteOn((bank*9)+6, 99, MIDI_CHANNEL);
	usbMIDI.sendNoteOff((bank*9)+6, 0, MIDI_CHANNEL);
}

/************************************************************
 * LONG PRESS												*
 ***********************************************************/
void handlerUpLongClicked() {
	Serial3.println("Up Long pressed");

	matrix.write(1, 7, 0);

	if (bank < 10) {
		bank = 10;
	} else {
		bank = maxBank;
	}
	needRedraw = true;
}

void handlerDownLongClicked() {
	Serial3.println("Down Long pressed");

	matrix.write(1, 7, 0);

	if (bank < 10) {
		bank = 0;
	} else {
		bank = 10;
	}
	needRedraw = true;
}

void handlerButton1LongClicked() {
	Serial3.println("Button 1 Long pressed");

	matrix.write(1, 7, 0);

	usbMIDI.sendNoteOn((bank*9)+7, 99, MIDI_CHANNEL);
	usbMIDI.sendNoteOff((bank*9)+7, 0, MIDI_CHANNEL);
}

void handlerButton2LongClicked() {
	Serial3.println("Button 2 Long pressed");

	matrix.write(1, 7, 0);

	usbMIDI.sendNoteOn((bank*9)+8, 99, MIDI_CHANNEL);
	usbMIDI.sendNoteOff((bank*9)+8, 0, MIDI_CHANNEL);
}

void handlerButton3LongClicked() {
	Serial3.println("Button 3 Long pressed");

	matrix.write(1, 7, 0);

	usbMIDI.sendNoteOn((bank*9)+9, 99, MIDI_CHANNEL);
	usbMIDI.sendNoteOff((bank*9)+9, 0, MIDI_CHANNEL);
}

void handlerLongPressIndicator() {
	matrix.write(1, 7, 1);
}

void setup() {
	Serial3.begin(9600);
	while(!Serial3);

	matrix.clear();
	matrix.setBrightness(1);

	//init buttons
	up.attachClick(handlerUpClicked);
	dn.attachClick(handlerDownClicked);
	b1.attachClick(handlerButton1Clicked);
	b2.attachClick(handlerButton2Clicked);
	b3.attachClick(handlerButton3Clicked);

	up.attachDoubleClick(handlerUpDoubleClicked);
	dn.attachDoubleClick(handlerDownDoubleClicked);
	b1.attachDoubleClick(handlerButton1DoubleClicked);
	b2.attachDoubleClick(handlerButton2DoubleClicked);
	b3.attachDoubleClick(handlerButton3DoubleClicked);

	up.attachLongPressStart(handlerLongPressIndicator);
	dn.attachLongPressStart(handlerLongPressIndicator);
	b1.attachLongPressStart(handlerLongPressIndicator);
	b2.attachLongPressStart(handlerLongPressIndicator);
	b3.attachLongPressStart(handlerLongPressIndicator);

	up.attachLongPressStop(handlerUpLongClicked);
	dn.attachLongPressStop(handlerDownLongClicked);
	b1.attachLongPressStop(handlerButton1LongClicked);
	b2.attachLongPressStop(handlerButton2LongClicked);
	b3.attachLongPressStop(handlerButton3LongClicked);
}

void loop() {
	up.tick();
	dn.tick();
	b1.tick();
	b2.tick();
	b3.tick();

	if (needRedraw == true) {
		Sprite *s = chars[bank];
		matrix.write(1, 1, *s);
		needRedraw = false;
	}
}