#pragma once

#include <Arduino.h>
#include <Sprite.h>
#include <OneButton.h>

class NamedButton : public OneButton {
	private:
		String _name = "";
	public:
		NamedButton(String name, int pin, int active, bool pullupActive = true, int debounceTime = 50):OneButton(pin, active, pullupActive) {
			_name = name;
		};

		void setButtonName(String name) {
			_name = name;
		}
		String getButtonName() {
			return _name;
		}
};

Sprite zero = Sprite(7, 5,
	B0111110,
	B1000001,
	B1001001,
	B1000001,
	B0111110
);

Sprite one = Sprite(7, 5,
	B0000000,
	B1000010,
	B1111111,
	B1000000,
	B0000000
);

Sprite two = Sprite(7, 5,
	B1000010,
	B1100001,
	B1010001,
	B1001001,
	B1000110
);

Sprite three = Sprite(7, 5,
	B0100010,
	B1000001,
	B1001001,
	B1001001,
	B0110110
);

Sprite four = Sprite(7, 5,
	B0010000,
	B0011000,
	B0010100,
	B1111111,
	B0010000
);

Sprite five = Sprite(7, 5,
	B1001111,
	B1001001,
	B1001001,
	B1001001,
	B0110001
);

Sprite six = Sprite(7, 5,
	B0111110,
	B1001001,
	B1001001,
	B1001001,
	B0110010
);

Sprite seven = Sprite(7, 5,
	B0000001,
	B0000001,
	B1110001,
	B0001101,
	B0000011
);

Sprite eight = Sprite(7, 5,
	B0110110,
	B1001001,
	B1001001,
	B1001001,
	B0110110
);

Sprite nine = Sprite(7, 5,
	B0100110,
	B1001001,
	B1001001,
	B1001001,
	B0111110
);

Sprite letter_a = Sprite(7, 5,
	B1111110,
	B0010001,
	B0010001,
	B0010001,
	B1111110
);

Sprite letter_b = Sprite(7, 5,
	B1111111,
	B1001001,
	B1001001,
	B1001001,
	B0110110
);

Sprite letter_c = Sprite(7, 5,
	B0111110,
	B1000001,
	B1000001,
	B1000001,
	B0100010
);

Sprite letter_d = Sprite(7, 5,
	B1111111,
	B1000001,
	B1000001,
	B1000001,
	B0111110
);

/*Sprite letter_e = Sprite(7, 5,
	B1111111,
	B1001001,
	B1001001,
	B1000001,
	B1000001
);

Sprite letter_f = Sprite(7, 5,
	B1111111,
	B0001001,
	B0001001,
	B0000001,
	B0000001
);

Sprite letter_g = Sprite(7, 5,
	B0111110,
	B1000001,
	B1001001,
	B1001001,
	B0111010
);

Sprite letter_h = Sprite(7, 5,
	B1111111,
	B0001000,
	B0001000,
	B0001000,
	B1111111
);

Sprite letter_i = Sprite(7, 5,
	B1000001,
	B1000001,
	B1111111,
	B1000001,
	B1000001
);

Sprite letter_j = Sprite(7, 5,
	B0100001,
	B1000001,
	B0111111,
	B0000001,
	B0000001
);

Sprite letter_k = Sprite(7, 5,
	B1111111,
	B0001000,
	B0010100,
	B0100010,
	B1000001
);

Sprite letter_l = Sprite(7, 5,
	B1111111,
	B1000000,
	B1000000,
	B1000000,
	B1000000
);

Sprite letter_m = Sprite(7, 5,
	B1111111,
	B0000010,
	B0001100,
	B0000010,
	B1111111
);

Sprite letter_n = Sprite(7, 5,
	B1111111,
	B0000100,
	B0001000,
	B0010000,
	B1111111
);

Sprite letter_o = Sprite(7, 5,
	B0111110,
	B1000001,
	B1000001,
	B1000001,
	B0111110
);

Sprite letter_p = Sprite(7, 5,
	B1111111,
	B0010001,
	B0010001,
	B0010001,
	B0001110
);*/

Sprite *chars[] = {
	&zero,
	&one,
	&two,
	&three,
	&four,
	&five,
	&six,
	&seven,
	&eight,
	&nine,
	&letter_a,
	&letter_b,
	&letter_c,
	&letter_d/*,
	&letter_e,
	&letter_f,
	&letter_g,
	&letter_h,
	&letter_i,
	&letter_j,
	&letter_k,
	&letter_l,
	&letter_m,
	&letter_n,
	&letter_o,
	&letter_p*/
};
