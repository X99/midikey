# But...why?

The MIDIKey project refers to one of my passions, music. As a bassist (and guitarist, but really a little bit!), I have at my disposal many effects: distortions, pitch shifters, equalizers... each one transforming the sound in a singular way.

Most of them are "physical" pedals, in the form of small metal boxes with various settings. For example here the famous Wahwah made popular by Jimmy Hendrix:

But in recent years, computers have made it possible to create digital effects, which can be integrated into a computer program. These programs are called "VST plugins" and allow to compose complex effects chains, easily to be used during the composition, or even during the interpretation on stage.

Although digital effects do not totally replace their physical equivalents, they have the advantage of saving space but also the plethora of settings that can be applied to them with a single click. A set of effects and their settings is called a "patch", and these patches can be used with the fingertips, directly on the computer. However, one problem remains: have you ever seen a guitarist, on stage, use his computer to change his effects? No, and that would be ridiculous! Indeed, besides the visual aspect, a computer is not made to be present in front of the stage, but rather in the background. So, to control his effects, the musician can have two solutions: the roady (also called "gear minion") or a MIDI controller (beware, one doesn't prevent the other).

A MIDI controller allows the musician to have, instead of a whole bunch of effects, only one controller. As far as I'm concerned, the MIDI controller has the advantage of being able to assign, to each button, an action such as "activates the tuner", "changes the patch" or simply "mutes the sound". Basically, the best of both worlds, with incredible flexibility!

As a good maker that I am, I first tried to build the controller rather than buying it. The MIDIKey project was born!

I used a TeensyLC microcontroller, which has the advantage of supporting the MIDI protocol via USB in a reduced form-factor. All I had to do was to add some bontons, a display and to put the whole thing in a solid case!

The finished product can generate 126 instructions, divided into 14 banks of 9 instructions each. This is made possible by the fact that each button allows 3 actions: single press, double press and long press. The top two buttons allow you to change banks (from 0 to D), the bottom three allow you to send a MIDI instruction via USB.

